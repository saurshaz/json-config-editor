import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyAb8dU1pl7T79Sy0cC0hCd1FD8O1Xl2GPk",
    authDomain: "reactandfirebase-4db48.firebaseapp.com",
    databaseURL: "https://reactandfirebase-4db48.firebaseio.com",
    projectId: "reactandfirebase-4db48",
    storageBucket: "reactandfirebase-4db48.appspot.com",
    messagingSenderId: "542111332976",
    appId: "1:542111332976:web:7b42372ffd8a5ea04d52b5",
    measurementId: "G-P2RFWJ2C3S"
}

firebase.initializeApp(config);
export default firebase;
